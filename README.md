# Canvas Percentage To Point Extension

> Not perfect, but it does the job.

![Example of effect](example.png)

## Introduction

This extension can convert percentages on the Canvas grades page to an end result on 20. This is developed for teachers of Artevelde University of Applied Sciences but can be used by everybody.

## Installation

Download this repository by pressing on the big blue button at the top right. Download the source code as a zip and unpack it somewhere on your computer where you won't delete it.

Open Chrome or your Chrome-based browser and go to extensions. Enable developer mode and press the 'Load unpacked button'. Select the unpacked zip folder you have stored locally on your device.

> Warning: If you move the files, the extension will stop working.

> More info on installing an unpacked extension: please follow [this guide](https://developer.chrome.com/docs/extensions/get-started/tutorial/hello-world#load-unpacked).

## Known issues

Scores that aren't visible when using the plugin won't be transformed. Just scroll down and press the button once more.

Also, when pressing the transform button multiple times, it will append the fraction every time.

All those bugs are fixable, but I made this in 20 minutes without any experience in Extension development, so I get a pass.
