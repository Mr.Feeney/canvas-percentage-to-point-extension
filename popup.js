// Copyright 2023 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const transformGradesButton = document.getElementById('transformGrades');


transformGradesButton.addEventListener('click', (event) => {
  // Query the active tab before injecting the content script
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    // Use the Scripting API to execute a script
    chrome.scripting.executeScript({
      target: { tabId: tabs[0].id },
      func: transformGradesToFractions
    });
  });
});

function transformGradesToFractions() {
  const totalCells = document.body.querySelectorAll('.slick-cell.total_grade .gradebook-cell .grades > .percentage:first-child');

  function stripAllHTMLtags(content) {
    return content.replace(/<i.*?<\/i>\s*|\s*%/g, '');;
  }

  function percentageToFraction(percentageStr, fractionBase = 20) {
    // Remove the '%' sign and convert to a number
    var decimalValue = parseFloat(stripAllHTMLtags(percentageStr)) / 100;
    // Calculate the numerator by multiplying by fractionBase
    var numerator = Math.round(decimalValue * fractionBase);

    // The denominator is always fractionBase for our case
    var denominator = fractionBase;

    // Return the formatted fraction string
    return `${numerator}/${denominator}`;
  }

  totalCells.forEach(cell => {
    cell.innerHTML = `${cell.innerHTML} (${percentageToFraction(cell.innerHTML)})`
  });
}
